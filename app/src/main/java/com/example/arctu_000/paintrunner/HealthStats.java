package com.example.arctu_000.paintrunner;

import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

public class HealthStats {

    private static final double METERS_IN_A_MILE = 1609.34;

    public enum Gender { MALE, FEMALE }

    //need these entries from a user page:
    public static final Gender gender = Gender.MALE;
    public static final int age = 22; //going to want to set this to type DATE once user input available
    public static final double weightLBS= 190, heightCM = 191; //for now my data

    public static double calculateDistance(double fromLong, double fromLat, double toLong, double toLat) {
        double d2r = Math.PI / 180;
        double dLong = (toLong - fromLong) * d2r;
        double dLat = (toLat - fromLat) * d2r;
        double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(fromLat * d2r)
                * Math.cos(toLat * d2r) * Math.pow(Math.sin(dLong / 2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 6367000 * c;
        return Math.round(d);
    }

    /**
     * Estimates speed in mph travelled between two locations using the start and end times
     * @param from location
     * @param to location
     * @param start time
     * @param end time
     * @return speed in mph
     */
    public static double calculateSpeed(LatLng from, LatLng to, long start, long end){
        //first section calculates distance travelled in miles
        double distanceInMeters = (calculateDistance(from.longitude, from.latitude, to.longitude, to.latitude));
        //uses distance travelled in miles to calculate miles per hour
        long difference = end - start;
//        double diffInHours = (double)(difference)/0.2777777;
        return ((distanceInMeters*60*60*1000)/(difference*METERS_IN_A_MILE));
    }

    private static double caloriesBurnedWalking(LatLng from, LatLng to, long start, long end){
        //TODO - FIX THIS CURRENTLY WRONG CALCULATIONS
        double distanceTravelled = calculateDistance(from.longitude, from.latitude, to.longitude, to.latitude)*METERS_IN_A_MILE;
        double burnedPerMile = 0.57*weightLBS;
        return burnedPerMile*distanceTravelled;
    }

    public static double calculateCaloriesBurned(long start, long end){
        //this caloric burn calculator is used for more intensive exercises such as biking or running
        if(gender==Gender.MALE){
            BigDecimal result = new BigDecimal((((age*0.2017) - (weightLBS*0.09036) + (((220-age) * 0.6309)) - 55.0969)*((double)(end-start)/60000.0) / 4.184), MathContext.DECIMAL64);
            return result.setScale(2, RoundingMode.DOWN).doubleValue();
        }else {
            BigDecimal result = new BigDecimal((((age*0.074) - (weightLBS*0.05741) + (((220-age) * 0.4472)) - 20.4022)*((double)(end-start)/60000.0) / 4.184), MathContext.DECIMAL64);
            return result.setScale(2, RoundingMode.DOWN).doubleValue();
        }
    }

}
