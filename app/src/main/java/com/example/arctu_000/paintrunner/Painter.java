package com.example.arctu_000.paintrunner;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import static com.example.arctu_000.paintrunner.HealthStats.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Painter extends FragmentActivity implements OnMapReadyCallback {

    public class Point<T, C, L> {
        private T time;
        private C color;
        private L latlng;

        public Point(T time, C color, L latlng) {
            this.time = time;
            this.color = color;
            this.latlng = latlng;
        }

        public T getTime() {
            return time;
        }

        public C getColor() {
            return color;
        }

        public L getLatLng() {
            return latlng;
        }

        public void setTime(T t) {
            time = t;
        }

        public void setColor(C c) {
            color = c;
        }

        public void setLatLng(L l) {
            latlng = l;
        }
    }

    private String resp=null;
    private int lineWidth=40, SQUARE_FOOTAGE =100;
    private ArrayList<Polyline> pastLines = new ArrayList<Polyline>();
    private Marker oldMarker;
    private ArrayList<Point<Long, Color, LatLng>> points, other_points, current_session_points;
    private ArrayList<Session> User_sessions;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private int grid_length_x, grid_length_y;   //lengths in feet of width and height of playing zone
    private LatLng UR_pos, UL_pos, LR_pos, LL_pos; //holds Upper, Right, Lower, and Left variations of positions for the border
    private String[][] grid;
    private int score, iterations=0;
    private Session session;
    private boolean previouslySpeeding;

    //static variables parsed from DB:
    public static String user = Login.Name.getText().toString();
    public static LatLng central_location = new LatLng(42.0000,-93.6319); //should instead find a way to parse this information.
    public static final double RUNNING_SPEED = 6;
    public static final int MIN_TIME_SESSION = 180000, MAX_SPEED = 29;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        points = new ArrayList<Point<Long, Color, LatLng>>();
        other_points = new ArrayList<Point<Long, Color, LatLng>>(); //parse other points into this
        current_session_points = new ArrayList<Point<Long, Color, LatLng>>();
        User_sessions = new ArrayList<Session>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painter);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                double longitude = 0;
                double latitude = 0;

                String message = null;
                if (location != null) {
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    message = user;
                }
                if (message != null) {
                    LatLng newPoint = new LatLng(latitude, longitude);
                    if(oldMarker!=null){
                        MarkerAnimation.animateMarkerToICS(oldMarker, newPoint, new LatLngInterpolator.Spherical()); //animation
                        oldMarker.hideInfoWindow();
                        oldMarker.remove();
                        //checks if last time taken was at least 3 minutes ago = break and end of session
                        if(current_session_points.size()>0 && ((System.currentTimeMillis() - current_session_points.get(current_session_points.size()-1).time) > MIN_TIME_SESSION)){
                            //TODO - SOMEHOW CHECK SPEED TOLERANCE AND MAKE SURE TO END SESSION IF TOO FAST
                            session.endSession(System.currentTimeMillis());
                            User_sessions.add(session);
                            session = new Session();
                            session.startSession(System.currentTimeMillis());
                        } else {
                            session.addSessionDistance(calculateDistance(oldMarker.getPosition().longitude, oldMarker.getPosition().latitude,
                                    longitude, latitude));
                            session.addCalories(calculateCaloriesBurned(session.getSessionStart(), System.currentTimeMillis()));
                        }
                    }

                    if(current_session_points.size()>0 && oldMarker!=null &&
                            withinSpeed(oldMarker.getPosition(), newPoint, current_session_points.get(current_session_points.size()-1).time, System.currentTimeMillis())){
                        updateGrid(newPoint);
                    }
                    updateScore();
                    if(current_session_points.size()==0 || (current_session_points.size()>0 && oldMarker!=null &&
                            withinSpeed(oldMarker.getPosition(), newPoint, current_session_points.get(current_session_points.size()-1).time, System.currentTimeMillis()))){
                            oldMarker = mMap.addMarker(new MarkerOptions().position(newPoint).title(message).snippet("Score: +" + score + "\nDistance: " +
                                session.getSession_distance() + " meters" + "\nCalories Burned: " + session.getCaloriesBurned()) //calories updated
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))); //added
                    }else { //going too fast
                        oldMarker = mMap.addMarker(new MarkerOptions().position(newPoint).title(message).snippet("Score: +" + score +
                                "\nVehicle play prohibited, session ended")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                        session.endSession(System.currentTimeMillis());
                        if(!previouslySpeeding){
                            User_sessions.add(session);
                        }
                        previouslySpeeding=true;
                        session = new Session();
                        session.startSession(System.currentTimeMillis());
                    }

                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }
                        @Override
                        public View getInfoContents(Marker marker) {
                            LinearLayout info = new LinearLayout(getApplicationContext());
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(getApplicationContext());
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());
                            TextView snippet = new TextView(getApplicationContext());
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());
                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });

                    oldMarker.showInfoWindow(); //added
                    OkHttpClient client= new OkHttpClient();
                    String name=Login.Name.getText().toString();
                    String password=Login.Password.getText().toString();
                    int col=Login.color;
                    String color=Integer.toHexString(col);
                    long time=System.currentTimeMillis();
                    double lat=latitude;
                    double lng=longitude;
                    String url = "http://10.27.170.174/paintrunnerserver/PRServer.php?type=post&name="+name+"&password="+password+"&color="+color+"&time="+time+"&lat="+lat+"&lng="+lng;
                    Log.d("catch",url);
                    Request request = new Request.Builder().url(url).build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                        }
                    });

                    if(points.size()>0) {
                        if(points.get(points.size()-1).getLatLng()!=null && insideScope(newPoint)) { //TODO - test insideScope
                                Polyline line = mMap.addPolyline(new PolylineOptions()
                                        .add(points.get(points.size() - 1).getLatLng(), newPoint)
                                        .width(lineWidth)
                                        .color(Color.parseColor('#' + color)));

                        }
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(newPoint));
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                    points.add(new Point(System.currentTimeMillis(), Color.CYAN, new LatLng(latitude, longitude)));
                    current_session_points.add(new Point(System.currentTimeMillis(), Color.CYAN, new LatLng(latitude, longitude)));
                    if(iterations%2==0){
                        OkHttpClient client2= new OkHttpClient();
                        String url2 = "http://10.27.170.174/paintrunnerserver/PRServer.php?type=get&name=esteban&password=esteban&color=orange&time=10&lat=42.3&lng=47.5";
                        Request request2 = new Request.Builder().url(url2).build();
                        client2.newCall(request2).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                resp=response.body().string();
                                //drawAllLines(response.body().string());
                            }
                        });
                    }
                    iterations++;
                    if(resp!=null){
                        drawAllLines(resp);
                        resp=null;
                    }
                } //if message != null end here
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) { }

            @Override
            public void onProviderEnabled(String s) { }

            @Override
            public void onProviderDisabled(String s) { }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        } else {

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2500, 1, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2500, 1, locationListener);
        //defines the end of the session:
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_SESSION, 0, locationListener);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        OkHttpClient client= new OkHttpClient();
        String url = "http://10.27.170.174/paintrunnerserver/PRServer.php?type=get&name=esteban&password=esteban&color=orange&time=10&lat=42.3&lng=47.5";
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                resp=response.body().string();
                //drawAllLines(response.body().string());
            }
        });
    }

    public void initializeGrid(LatLng latLng){
        //determines number of 100' x 100' squares present at the current zoom level around the central location
        double ftperpx = 3.28084*(156543.03392 * Math.cos(latLng.latitude * Math.PI / 180) / Math.pow(2, 15.0));
        //determines border for game area
        double meters = (((ftperpx/3.28084)*1080)/2);   //finds the additional meters to add to the central location
        double coef = meters * 0.0000089;
        double new_lat_r = latLng.latitude + coef;
        double new_lat_l = latLng.latitude - coef;
        meters = (((ftperpx/3.28084)*1920)/2);
        coef = meters * 0.0000089;
        double new_long_l, new_long_r;
        if(latLng.longitude<=0) {
            new_long_l = latLng.longitude + coef / Math.cos(latLng.latitude * 0.018);
            new_long_r = latLng.longitude - coef / Math.cos(latLng.latitude * 0.018);
        }else {
            new_long_l = latLng.longitude - coef / Math.cos(latLng.latitude * 0.018);
            new_long_r = latLng.longitude + coef / Math.cos(latLng.latitude * 0.018);
        }
        UL_pos = new LatLng(new_lat_r, new_long_r);
        UR_pos = new LatLng(new_lat_r, new_long_l);
        LL_pos = new LatLng(new_lat_l, new_long_r);
        LR_pos = new LatLng(new_lat_l, new_long_l);

        //draws borders:
        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                .add(UL_pos, UR_pos, LR_pos, LL_pos)
                .strokeColor(Color.BLACK)
                .fillColor(Color.TRANSPARENT)
                .strokeJointType(JointType.BEVEL)
                .strokeWidth(30));

        grid_length_x = (int) ((calculateDistance(UL_pos.longitude, UL_pos.latitude, UR_pos.longitude, UR_pos.latitude)*3.28084)/SQUARE_FOOTAGE)+1;
        grid_length_y = (int) ((calculateDistance(UL_pos.longitude, UL_pos.latitude, UL_pos.longitude, LL_pos.latitude)*3.28084)/SQUARE_FOOTAGE)+1;

        //sets the number of available grid squares at the current zoom level - filled with nulls
        grid = new String[grid_length_y][grid_length_x];
    }

    public void updateGrid(LatLng latLng){
        double ft2left = 3.28084*calculateDistance(latLng.longitude, UL_pos.latitude, UL_pos.longitude, UL_pos.latitude);
        double ft2top = 3.28084*calculateDistance(UL_pos.longitude, latLng.latitude, UL_pos.longitude, UL_pos.latitude);
        if(grid_length_y != 0 && grid_length_x !=0 && (insideScope(latLng))){
            int y = (int)ft2top/SQUARE_FOOTAGE;
            if(ft2top%SQUARE_FOOTAGE!=0){
                y++;
            }
            int x = (int)ft2left/SQUARE_FOOTAGE;
            if(ft2top%SQUARE_FOOTAGE!=0){
                x++;
            }
            grid[y][x]=user;
        }else {
            Toast.makeText(getApplicationContext(),
                    "Return to the canvas Painter",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void updateScore(){

        //TODO - need to take in DB data and load it into the grid by time
        //TODO - update score multiplicatively for each area under control by a factor of t where t = time_controlled
        int original = score;
        if(grid_length_y != 0 && grid_length_x !=0){
            int counter=0;
            for(int i=0; i<grid_length_y; i++){
                for(int j=0;j<grid_length_x;j++){
                    if(grid[i][j]!=null && grid[i][j].equals(user)){
                        counter++;
                    }
                }
            }
            score = counter;
        }
        if(score != original) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Updated SCORE: " + score,
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * returns if the current LatLng position is inside the canvas
     * @param currentPosition
     * @return
     */
    public boolean insideScope(LatLng currentPosition){
        //TODO - add check for if LatLng latitude or longitude are positive or

        //if UR_pos !=null then all of the corner positions !=null
        return(UR_pos!=null &&
                (UL_pos.longitude <= currentPosition.longitude && UR_pos.longitude >=currentPosition.longitude &&
                UR_pos.latitude>=currentPosition.latitude && LL_pos.latitude<=currentPosition.latitude));
    }

    /**
     * Tolerance that checks if the speed of the person is within a satisfiable range
     * @param from
     * @param to
     * @param start
     * @param end
     * @return
     */
    public boolean withinSpeed(LatLng from, LatLng to, long start, long end){
        return(MAX_SPEED>calculateSpeed(from, to, start, end));
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        score=0;
        initializeGrid(central_location);
        session = new Session();
        session.startSession(System.currentTimeMillis());
        previouslySpeeding = false;

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(central_location));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void drawAllLines(String json){
        Log.d("catch",json);
        for(int i=0;i<pastLines.size();i++){
            pastLines.get(i).remove();
        }
        JSONArray reader = null;
        try {
            reader = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        };
        for(int i=0;i<reader.length();i++){//foreach name in the array.
            try {
                Log.d("catch",reader.get(i).toString());
                JSONObject temp = new JSONObject(reader.get(i).toString());
                Log.d("catch",temp.getString("name"));
                Log.d("catch",temp.getString("color"));
                JSONArray temparr = new JSONArray(temp.getString("data"));
                for(int j=0;j<temparr.length()-1;j++){
                    Log.d("catch",temparr.get(j).toString());
                    JSONObject tll = new JSONObject(temparr.get(j).toString());
                    JSONObject tll2 = new JSONObject (temparr.get(j+1).toString());
                    LatLng newPoint1 = new LatLng(tll.getDouble("lat"), tll.getDouble("lng"));
                    LatLng newPoint2 = new LatLng(tll2.getDouble("lat"), tll2.getDouble("lng"));
                    String col=temp.getString("color");
                    if(col.charAt(0)!='#'){
                        col='#'+col;
                    }
                    if(insideScope(newPoint2)) { //TODO - TEST THIS STATEMENT
                        Polyline pl = mMap.addPolyline(new PolylineOptions()
                                .add(newPoint1, newPoint2)
                                .width(lineWidth)
                                .color(Color.parseColor(col)));
                        pastLines.add(pl);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

