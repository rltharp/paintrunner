package com.example.arctu_000.paintrunner;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Random;


public class Login extends AppCompatActivity
{
    public static EditText Name ;
    public static EditText Password;
    private Button Login;
    private int count = 5;
    private TextView Info;

    //Random Color
    private static Random rnd = new Random();
    public static int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Name = (EditText) findViewById(R.id.etName);
        Password = (EditText) findViewById(R.id.etPassword);
        Login = (Button) findViewById(R.id.button);
        Info = (TextView) findViewById(R.id.tvInfo);
        Info.setText("No. of attempts remaining: 5");

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(Name.getText().toString(), Password.getText().toString());
            }
        });
    }

    private void validate(String userName, String userPassword)
    {   //edit line below to parse through array of users
   //     if((userName.equals("doit")) && (userPassword.equals("1234"))) {
            Intent i = new Intent(Login.this, Painter.class);
            startActivity(i);
//        }
//        else {
//            count--;
//            Info.setText("No. of attempts remaining: " + String.valueOf(count));
//
//            if(count == 0){
//                Login.setEnabled(false);
//            }
//        }
    }

}
