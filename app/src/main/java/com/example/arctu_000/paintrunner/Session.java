package com.example.arctu_000.paintrunner;

import static com.example.arctu_000.paintrunner.HealthStats.*;

public class Session {
    private double session_distance;
    private long sessionStart, sessionEnd;
    private double caloriesBurned;

    public Session(){
        session_distance=0;
        caloriesBurned = 0;
        sessionStart =0;
        sessionEnd = 0;
    }

    public void startSession(long time){ sessionStart=time; }

    public long getSessionStart() { return sessionStart; }

    public long getSessionEnd() { return sessionEnd; }

    public void addSessionDistance(double distance){ session_distance+=distance; }

    public double getSession_distance(){ return session_distance; }

    public void addCalories(double calories) { caloriesBurned = calories; }

    public double getCaloriesBurned() { return caloriesBurned; }

    public void endSession(long time){
        sessionEnd = time;
        addCalories(calculateCaloriesBurned(sessionStart, sessionEnd));
        //TODO - final calculations and plotting of data
    }
}
