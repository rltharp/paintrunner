<?php
	if($_GET["type"]=="post"){
		$name=$_GET["name"];
		$password=$_GET["password"];
		$color=$_GET["color"];
		//Build an object, then stringify and store
		//file's name is the username
		//If not username found, create a file
		//then two single instances: color and password
		//if password doesn't match, return error
		//store an arraylist of (lat, long, and time)
		$time=$_GET["time"];
		$lat=$_GET["lat"];
		$lng=$_GET["lng"];
		$file = fopen($name.".txt", "r");
		
		//if file exists, build on it
		if($file!=null){
			$data = fread($file,filesize($name.".txt"));
			fclose($file);
			$obj=json_decode($data,true);
			if($obj["password"]==$password){
				$wfile=fopen($name.".txt", "w");
				$obj["data"][] = (object) array(
					"time"=>$time,
					"lat"=>$lat,
					"lng"=>$lng
					);
				fwrite($wfile,json_encode($obj));
			}else{
				echo "incorrect credentials";
			}
			
			
			echo($data);
		}else{//file doesn't exist, create new object then store it
			$obj=new stdClass();
			$obj->name=$name;
			$obj->password=$password;
			$obj->color=$color;
			$obj->data=(object) array((object) array(
					"time"=>$time,
					"lat"=>$lat,
					"lng"=>$lng
			));
			$temp = json_encode($obj);
			$file2=fopen($name.".txt",'w');
			echo fwrite($file2,$temp);
		}
	}else if($_GET["type"]=="get"){
		//Build an object of all the current routes, let the client
		//Worry about which time stamps to actually update.
		//list directory names, grab .txt files, send all their data
		//back to requester, except for passwords.
		$files=scandir('.');
		$obj=array();
		foreach($files as $sfile){
			if($sfile{strlen($sfile)-1}=='t'){
				$opened=fopen($sfile, "r");
				$obj[]=fread($opened,filesize($sfile));
				fclose($opened);
			}
		}
		echo(json_encode($obj));
	}else{
		echo "unknown command";
	}
?> 